# [Deprecated]
This implementation has bean deprecated due to some issues with the project structure that I didn't think beforehand.
Please visit https://github.com/CoolDeveloper101/simple-math-interpreter to view to new project.

# Math Interpreter


This is a simple math interpreter written in c#. Thanks to [CodePulse](https://www.youtube.com/channel/UCUVahoidFA7F3Asfvamrm7w) for the tutorials to make a simple math interpreter. You can check out original the source code [here](https://github.com/davidcallanan/py-simple-math-interpreter). I basically converted it from python to c#. Thanks to CodePulse I was able to create this project.

## Credits

Thanks to [CodePulse](https://www.youtube.com/channel/UCUVahoidFA7F3Asfvamrm7w) for this project. He created the videos for this project in which he made this interpreter in python.
I just converted it to c# code.

## Build from Source

This project is written in .NET Core, so it can be used on Windows, Mac or Linux.

Follow the given steps to build the project from source:

1. Clone the repository.
2. Open the terminal.
3. Navigate to the directory `src/Math Interpreter` directory in the repository.
4. Run the command - `dotnet run` in the terminal. (You should have .NET Core SDK installed)

You can use it from the command line as well:

1. Build the project.
2. Navigate to the directory that conatins the executable.
3. Run the excecutable in the terminal using the -e option followed by an expression (For example "Math Interpreter" -e '12 + 1 - 4')

There is another way to supply command line arguments to the project:

1. Navigate to the directory `src/Math Interpreter` directory in the repository.
2. Run the command `dotnet run -- -e '2 - 12 + 23'`. We use the '--' option in dotnet to supply arguments to the command line.


## Usage
The interpreter processes expressions in the following hierarchy -

**B** - Brackets '( )'<br>
**E** - Exponents '**'<br>
**D** - Division '/'<br>
**M** - Multiplication '*'<br>
**A** - Addition '+'<br>
**S** - Subtraction '-'<br>

When you run the program, it prompts you for input.<br>
You can enter any valid mathematical operation containing digits, brackets or the power, division, multiplication, addition and subtraction operators

### Example

`>>> 1 + 6 ** 2 / 6 * 2`<br>
The output will be:<br>
`13`
