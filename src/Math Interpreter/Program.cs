﻿using System;
using System.Collections.Generic;

namespace Math_Interpreter
{
    class Program
    {
        static int Main(string[] args)
        {
            int exitCode = 0;
            if (args.Length == 0)
                Interpret();

            switch (args[0])
            {
                case "-e":
                    switch (args.Length)
                    {
                        case 2:
                            try
                            {
                                Lexer lexer = new Lexer(args[1]);
                                List<Token> tokens = lexer.GetTokens();
                                //Console.WriteLine($"[{string.Join(", ", tokens)}]");
                                Parser parser = new Parser(tokens);
                                Node tree = parser.Parse();
                                //Console.WriteLine(tree);
                                if (tree.nodeType != NodeType.EmptyNode)
                                {
                                    Interpreter interpreter = new Interpreter();
                                    Number result = interpreter.Visit(tree);
                                    Console.WriteLine(result);
                                }
                            }
                            catch (Exception e)
                            {
                                Console.Error.WriteLine(e);
                                exitCode = 1;
                            }
                            break;
                        
                        case 1:
                            Console.Error.WriteLine("Option -e takes atleast 1 argument.");
                            exitCode =  1;
                            break;
                        default:
                            Console.Error.WriteLine("Option -e takes only 1 argument.");
                            break;
                    }
                    break;

                case "-h":
                    Console.WriteLine(@"usage: Math Interpreter [-e <expression>] [-h|--help help]");
                    break;

                case "--help":
                    Console.WriteLine(@"usage: Math Interpreter [-e <expression>] [-h|--help help]");
                    break;
                
                default:
                    Console.Error.WriteLine($"Invalid option: {args[0]}");
                    exitCode = 1;
                    break;
            }
            return exitCode;
        }

        static void Interpret()
        {
            while (true)
            {
                try
                {
                    Console.Write(">>> ");
                    string text = Console.ReadLine();
                    Lexer lexer = new Lexer(text);
                    List<Token> tokens = lexer.GetTokens();
                    //Console.WriteLine($"[{string.Join(", ", tokens)}]");
                    Parser parser = new Parser(tokens);
                    Node tree = parser.Parse();
                    //Console.WriteLine(tree);
                    if (tree.nodeType != NodeType.EmptyNode)
                    {
                        Interpreter interpreter = new Interpreter();
                        Number result = interpreter.Visit(tree);
                        Console.WriteLine(result);
                    }
                }
                catch (Exception e)
                {
                    Console.Error.WriteLine(e);
                }
            }
        }
    }
}
